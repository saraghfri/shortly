import Statistics from "./components/Statistics";
import Shortly from "./components/Shortly.vue"

export const routes = [
    {
        name:'statistics',
        path: '/',
        component: Statistics,
    }, {
        name:'shortly',
        path: '/shortly/:shortedLink',
        component: Shortly,
    }
];